/**
 * New node file
 */

var Board = require('../src/Board.js'),
	should = require("should");

describe('Board', function() {
	var b, b2;

	beforeEach(function() {
		b = new Board();
	});
	
	describe('Creator and reading/writing', function() {
		it('creator without params should init to clean Board', function() {
			b.countEmptyCells().should.equal(81);
			b.candTable[9].size.should.equal(81);
		});
		it('toSudokuString', function() {
			for (var i = 0; i < 9; i++)
				b.setCellValue(i,i,i+1).should.equal(true);
			var s = b.toSudokuString();
			s.should.equal("100000000020000000003000000000400000000050000000006000000000700000000080000000009");
			b.countEmptyCells().should.equal(81 - 9);
		});
		it('readFromSudokuString', function() {
			b.readFromSudokuString("000000001000000002003004000000000350000060000078010000000903400020005000710000000");
			b.countEmptyCells().should.equal(81 - 17);
		});
	});

	describe('setCellValue()', function() {
		it('should reduce candidates in all directions 1', function() {
			// Set value row 4, col 5, newval 6
			// This should prevent using same number in
			//  - whole row 4
			//  - whole column 5
			//  - whole block (3,3) to (5,5) 
			b.setCellValue(4,5,6).should.equal(true);
			b.setCellValue(1,5,6).should.equal(false);
			b.setCellValue(4,8,6).should.equal(false);
			b.setCellValue(3,3,6).should.equal(false);
			b.setCellValue(5,5,6).should.equal(false);
			b.countEmptyCells().should.equal(80);
		});
		it('should reduce candidates in all directions 2', function() {
			// Set value row 4, col 5, newval 6
			// This should prevent using same number in
			//  - whole row 4
			//  - whole column 5
			//  - whole block (3,3) to (5,5) 
			b.setCellValue(4,5,6).should.equal(true);
			b.setCellValue(1,5,6).should.equal(false);
			b.setCellValue(4,8,6).should.equal(false);
			b.setCellValue(3,3,6).should.equal(false);
			b.setCellValue(5,5,6).should.equal(false);
			
			b.setCellValue(4,4,2).should.equal(true);
			b.setCellValue(1,4,2).should.equal(false);
			b.setCellValue(4,8,2).should.equal(false);
			b.setCellValue(3,3,2).should.equal(false);
			b.setCellValue(5,5,2).should.equal(false);
			
			b.countEmptyCells().should.equal(79);
		});
		it('should be possible to fill separate values for each cell in a block', function() {
			var r, c;
			for (r = 0; r < 3; r++)
				for (c = 0; c < 3; c++)
					b.setCellValue(r,c,(r*3 + c + 1)).should.equal(true);
			b.setCellValue(0,5,1).should.equal(false);
			b.setCellValue(4,1,5).should.equal(false);
			b.setCellValue(3,3,1).should.equal(true);
			b.setCellValue(5,5,9).should.equal(true);
			for (r = 6; r < 9; r++)
				for (c = 6; c < 9; c++)
					b.setCellValue(r,c,((r-6)*3 + (c-6) + 1)).should.equal(true);
			b.toSudokuString().should.equal("123000000456000000789000000000100000000000000000009000000000123000000456000000789");
			b.countEmptyCells().should.equal(81 - 9 - 9 - 2);
		});
		it('equals', function() {
			b2 = new Board();
			b.readFromSudokuString("000000001000000002003004000000000350000060000078010000000903400020005000710000000");
			b2.readFromSudokuString("000000001000000002003004000000000350000060000078010000000903400020005000710000000");
			b.equals(b2).should.equal(true);
		});
		it('not equals', function() {
			b2 = new Board();
			b.readFromSudokuString("000000001000000002003004000000000350000060000078010000000903400020005000710000000");
			b.equals(b2).should.equal(false);
		});
		it('failed setCellValue() should not modify board', function() {
			// Set value row 4, col 5, newval 6
			// This should prevent using same number in
			//  - whole row 4
			//  - whole column 5
			//  - whole block (3,3) to (5,5) 
			b.setCellValue(4,5,6).should.equal(true);
			b2 = b.clone();
			b.setCellValue(1,5,6).should.equal(false);
			b.equals(b2).should.equal(true);
		});
	});

	describe('cellRemoveCandidate()', function() {
		it('should move cell in candidates map', function() {
			b.candTable[9].size.should.equal(81);
			b.cellRemoveCandidate(4,4,5);
			b.candTable[9].size.should.equal(80);
			b.candTable[8].size.should.equal(1);
			b.cellRemoveCandidate(4,4,1);
			b.cellRemoveCandidate(4,4,9);
			b.candTable[8].size.should.equal(0);
			b.candTable[6].size.should.equal(1);
			
			b.setCellValue(4,4,6).should.equal(true);
			b.candTable[6].size.should.equal(0);
		});
		it('candidates in map should be iterable', function() {
			cells = [];
			cells.push(b.b[4][4]);
			cells.push(b.b[1][1]);
			cells.push(b.b[8][8]);
			for (var i = 2; i <= 9; i++) {
				b.cellRemoveCandidate(4,4,i);
				b.cellRemoveCandidate(1,1,i);
				b.cellRemoveCandidate(8,8,i);
			}
			b.candTable[1].size.should.equal(3);
			b.candTable[2].size.should.equal(0);
			b.candTable[9].size.should.equal(78);
			var cands = b.candTable[1].values();
			for (var c = 0; c < cands.length; c++) {
				var cnd = b.lookupCellByRC(cands[c]);
				var cll = cells[c];
				cnd.should.equal(cll);
			}
		});
	});
});
