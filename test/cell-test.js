/**
 * Cell tests
 */

var Cell = require('../src/cell.js'),
	should = require("should");

describe('Cell', function() {
	var cell;
	
	describe('Creator and clone()', function() {
		it('creator without params should init to unset Cell with all candidates', function() {
			cell = new Cell();
			cell.isSet().should.equal(false);
			for (var cand = 1; cand <= 9; cand++)
				cell.hasCandidate(cand).should.equal(true);
		});
		it('creator newval = 0 should init to unset Cell with all candidates', function() {
			cell = new Cell(0);
			cell.isSet().should.equal(false);
			for (var cand = 1; cand <= 9; cand++)
				cell.hasCandidate(cand).should.equal(true);
		});
		it('creator newval = 5 should init to set Cell with value of 5 and all candidates unset', function() {
			cell = new Cell(5);
			cell.isSet().should.equal(true);
			cell.value.should.equal(5);
			for (var cand = 1; cand <= 9; cand++)
				cell.hasCandidate(cand).should.equal(false);
		});
		it('cloning should preserve value', function() {
			cell = new Cell(5);
			var cell2 = cell.clone();
			cell2.value.should.equal(5);
			for (var cand = 1; cand <= 9; cand++)
				cell.hasCandidate(cand).should.equal(false);
		});
		it('cloning should preserve candidates', function() {
			cell = new Cell();
			cell.removeCandidate(6);
			cell.removeCandidate(7);
			var cell2 = cell.clone();
			cell2.isSet().should.equal(false);
			cell.hasCandidate(6).should.equal(false);
			cell.hasCandidate(7).should.equal(false);
			for (var cand = 1; cand <= 9; cand++)
				if (cand != 6 && cand != 7)
					cell.hasCandidate(cand).should.equal(true);
		});
		it('equals', function() {
			cell = new Cell();
			cell.removeCandidate(6);
			cell.removeCandidate(7);
			var cell2 = cell.clone();
			cell.equals(cell2).should.equal(true);
		});
		it('not equals on candidates', function() {
			cell = new Cell();
			cell.removeCandidate(6);
			cell.removeCandidate(7);
			var cell2 = cell.clone();
			cell2.removeCandidate(1);
			cell.equals(cell2).should.equal(false);
		});
		it('not equals on value', function() {
			cell = new Cell();
			cell.removeCandidate(1);
			cell.removeCandidate(9);
			var cell2 = cell.clone();
			cell2.setValue(5);
			cell.equals(cell2).should.equal(false);
		});
	});
	
	describe('Set value and candidates', function() {
		it('set value should reset all candidates', function() {
			cell = new Cell();
			cell.isSet().should.equal(false);
			cell.setValue(4);
			cell.isSet().should.equal(true);
			cell.value.should.equal(4);
			cell.numCandidates().should.equal(0);
			for (var cand = 1; cand <= 9; cand++)
				cell.hasCandidate(cand).should.equal(false);
		});
		it('removing one candidate should leave all others untouched', function() {
			cell = new Cell();
			cell.hasCandidate(3).should.equal(true);
			cell.removeCandidate(3);
			cell.hasCandidate(3).should.equal(false);
			cell.isSet().should.equal(false);
			for (var cand = 1; cand <= 9; cand++)
				if (cand != 3)
					cell.hasCandidate(cand).should.equal(true);
		});
		it('numCandidates() should return number of candidates', function() {
			cell = new Cell();
			cell.numCandidates().should.equal(9);
			cell.removeCandidate(3).should.equal(true);
			cell.numCandidates().should.equal(8);
			cell.removeCandidate(3).should.equal(false);
			cell.numCandidates().should.equal(8);
			cell.removeCandidate(1).should.equal(true);
			cell.removeCandidate(9).should.equal(true);
			cell.removeCandidate(6).should.equal(true);
			cell.numCandidates().should.equal(5);
		});
	});
	

});
