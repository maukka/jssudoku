/**
 * Tests for all solvers
 *
 *	Most of test sudokus exported from http://mypuzzle.org/sudoku
 *	Some are from http://www.sudokuoftheday.com
 *
 *  Run tests with "npm test"
 */

var Board = require('../src/board.js'),
	BruteForceSolver = require('../src/brute-force-solver.js'),
	OptimizedSolver = require('../src/optimized-solver.js'),
	should = require("should");

describe('Solver tests', function() {
	var b;
	
	describe('BruteForceSolver', function() {
		var bfs;
		
		beforeEach(function() {
			b = new Board();
			bfs = new BruteForceSolver(b);
		});
		
		it('Simple board solvable with single candidates - 1 solution', function() {

			// Simple board solvable with single candidates
			// +-------+-------+-------+
			// | 2 . 7 | 1 . 8 | 3 . 9 |
			// | 3 . . | 6 2 9 | . . 7 |
			// | . 6 . | 3 . 7 | . 5 . |
			// +-------+-------+-------+
			// | . 1 2 | . . . | 6 9 . |
			// | 5 . . | . 8 . | . . 1 |
			// | 8 . . | . 9 . | . . 4 |
			// +-------+-------+-------+
			// | . 2 1 | . 3 . | 7 8 . |
			// | 7 3 4 | 8 . 5 | 9 2 6 |
			// | 9 8 . | . . . | . 1 3 |
			// +-------+-------+-------+
			b.readFromSudokuString("207108309300629007060307050012000690500080001800090004021030780734805926980000013");
			bfs.run().should.equal(true);
			b.toSudokuString().should.equal("247158369358629147169347852412573698593486271876291534621934785734815926985762413");
		});

		it('Easy board with hidden singles - 1 solution', function() {
		
			// Easy board with hidden singles
			// +-------+-------+-------+
			// | . 3 . | 2 . 8 | . 7 . |
			// | . . . | 1 . 9 | . . 3 |
			// | 2 8 . | . 6 7 | . . 1 |
			// +-------+-------+-------+
			// | . 2 . | . . 3 | . 1 . |
			// | 7 . . | 5 . 2 | . . 6 |
			// | . 5 . | 8 . 6 | . 9 . |
			// +-------+-------+-------+
			// | 5 . . | . 8 4 | . 6 7 |
			// | 8 . . | 7 . 1 | . . . |
			// | . 4 . | 6 . 5 | . 2 . |
			// +-------+-------+-------+
			b.readFromSudokuString("030208070000109003280067001020003010700502006050806090500084067800701000040605020");
			bfs.run().should.equal(true);
			b.toSudokuString().should.equal("135248679476159283289367541628493715794512836351876492512984367863721954947635128");
		});
		
		it('Simple board - > 500 solutions, hope that this algo picks this one', function() {
			b.readFromSudokuString("........2..8..7..5.4..2..7.......9...6..9....7...8.....23....5....4.....5..3.9.6.");
			bfs.run().should.equal(true);
			b.toSudokuString().should.equal("137546892298137645645928173352614987864793521719285436423861759976452318581379264");
		});
		
	});
	
	describe('OptimizedSolver', function() {
		var os;
				
		beforeEach(function() {
			b = new Board();
			os = new OptimizedSolver(b);
		});
		
		it('Mild board - 1 solution', function() {
			b.readFromSudokuString("348000079100008000002000100003060000400907003000020800001000900000600002750000318");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("348156279169278435572349186213864597485917623697523841821735964934681752756492318");
		});
		it('Moderate board - 1 solution', function() {
			b.readFromSudokuString("006130500007009060300000000490005086000000000870900014000000007030800600008054200");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("246137598157289463389546172492315786613478925875962314521693847734821659968754231");
		});
		it('Hard board - 1 solution', function() {
			b.readFromSudokuString("015000600300120000470008000002400080100000002080003400000700094000051007006000350");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("215379648368124579479568123932415786154687932687293415521736894843951267796842351");
		});
		it('Very Hard board - 1 solution', function() {
			b.readFromSudokuString("100306009000070200000005040200800406070000030805003001050700000003020000400901002");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("127346589548179263369285147231897456674512938895463721952738614713624895486951372");
		});
		it('Arto Inkala', function() {
			b.readFromSudokuString("800000000003600000070090200050007000000045700000100030001000068008500010090000400");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("812753649943682175675491283154237896369845721287169534521974368438526917796318452");
		});
		it('Unsolvable #49, sudokuwiki.org', function() {
			b.readFromSudokuString("002800000030060007100000040600090000050600009000057060000300100070006008400000020");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("742835691539461287186972543618293754257684319394157862825349176971526438463718925");
		});

		// "Unsolvable #28, sudokuwiki.org - takes 30 seconds to complete in unit tests
		it('Unsolvable #28, sudokuwiki.org (takes 30 sek with phoneApp) ', function() {
			b.readFromSudokuString("600008940900006100070040000200610000000000200089002000000060005000000030800001600");
			os.run().should.equal(true);
			b.toSudokuString().should.equal("625178943948326157371945862257619384463587291189432576792863415516294738834751629");
		});

//		testBoards[2] = Board("007405080040070000500008000023010054490300801075080023600007000050040000004603090");
//		testBoardsSolved[2] = Board("217465389348179562569238417823716954496352871175984623631597248952841736784623195");
//		testBoards[3] = Board("230050097070060050581409236100000005860295041700804003000080000007521800900746002");
//		testBoardsSolved[3] = Board("236158497479362158581479236124637985863295741795814623612983574347521869958746312");
//		testBoards[4] = Board("030006080000008045000240009060800074002534800480001050200065000610400000040900060");
//		testBoardsSolved[4] = Board("934156782721398645856247319563829174172534896489671253298765431615483927347912568");
//		testBoards[5] = Board("100058963000009057039600200370400008482036095690700001013500800000003019800014372");
//		testBoardsSolved[5] = Board("147258963268349157539671284371495628482136795695782431913527846724863519856914372");
//
//		testBoards[6] = Board("000259000200040001060000020510807069090000080070090050400080006005060800700405003");
//		testBoardsSolved[6] = Board("147259638258346791369178425514837269692514387873692154421983576935761842786425913");
	
	
	});
});
