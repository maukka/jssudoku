/**
 * Unit tests for BitSet9 module
 */
var BitSet9 = require('../src/bitset9.js'),
	should = require("should");

describe('BitSet9', function() {
	var bs, bs2;
	
	describe('init()', function() {
		it('should init with all bits set', function() {
			bs = new BitSet9();
			for (var pos = 1; pos <= 9; pos++) {
				bs.isSet(pos).should.equal(true);
			}
		});
		it('should clone with old bits', function() {
			bs = new BitSet9();
			bs.unSet(1);
			bs.unSet(5);
			var bs2 = new BitSet9(bs);
			bs2.isSet(1).should.equal(false);
			bs2.isSet(5).should.equal(false);
			for (var pos = 1; pos <= 9; pos++) {
				if (pos != 1 && pos != 5)
				bs2.isSet(pos).should.equal(true);
			}
		});
		it('should be separate entities after cloning', function() {
			bs = new BitSet9();
			var bs2 = new BitSet9(bs);
			bs2.unSet(7);
			bs.isSet(7).should.equal(true);
			bs2.isSet(7).should.equal(false);
		});
	});

	describe('clear()', function() {
		it('all bits should clear', function() {
			bs = new BitSet9();
			bs.clear();
			for (var pos = 1; pos <= 9; pos++) {
				bs.isSet(pos).should.equal(false);
			}
		});
	});

	describe('count()', function() {
		it('should return number of bits set', function() {
			bs = new BitSet9();
			bs.count().should.equal(9);
			bs.unSet(1);
			bs.count().should.equal(8);
			bs.unSet(9);
			bs.unSet(6);
			bs.count().should.equal(6);
			bs.clear();
			bs.count().should.equal(0);
		});
	});

	describe('unSet()', function() {
		it('one bit should unSet', function() {
			bs = new BitSet9();
			bs.isSet(4).should.equal(true);
			bs.unSet(4);
			bs.isSet(4).should.equal(false);
			bs.isSet(1).should.equal(true);
			bs.isSet(5).should.equal(true);
			bs.isSet(9).should.equal(true);
		});
		it('should return false if was already unset', function() {
			bs = new BitSet9();
			bs.isSet(4).should.equal(true);
			bs.unSet(4).should.equal(true);
			bs.unSet(4).should.equal(false);
			bs.isSet(9).should.equal(true);
			bs.unSet(9).should.equal(true);
			bs.unSet(9).should.equal(false);
			bs.isSet(9).should.equal(false);
		});
	});

	describe('getNext()', function() {
		it('test iterator', function() {
			bs = new BitSet9();
			bs.unSet(1);
			bs.unSet(3);
			bs.unSet(6);
			bs.unSet(7);
			bs.unSet(9);
			bs.getNext().should.equal(2);
			bs.getNext(0).should.equal(2);
			bs.getNext(2).should.equal(4);
			bs.getNext(4).should.equal(5);
			bs.getNext(5).should.equal(8);
			bs.getNext(8).should.equal(0);
		});
		it('should return false if was already unset', function() {
			bs = new BitSet9();
			bs.isSet(4).should.equal(true);
			bs.unSet(4).should.equal(true);
			bs.unSet(4).should.equal(false);
			bs.isSet(9).should.equal(true);
			bs.unSet(9).should.equal(true);
			bs.unSet(9).should.equal(false);
			bs.isSet(9).should.equal(false);
		});
	});

	describe('equals()', function() {
		it('null sets equal', function() {
			bs = new BitSet9();
			bs2 = new BitSet9();
			bs.clear();
			bs2.clear();
			bs.equals(bs2).should.equal(true);
		});
		it('full sets equal', function() {
			bs = new BitSet9();
			bs2 = new BitSet9();
			bs.equals(bs2).should.equal(true);
		});
		it('specific sets equal', function() {
			bs = new BitSet9();
			bs2 = new BitSet9();
			bs.unSet(2);
			bs.unSet(7);
			bs2.unSet(2);
			bs2.unSet(7);
			bs.equals(bs2).should.equal(true);
		});
		it('different sets do not equal', function() {
			bs = new BitSet9();
			bs2 = new BitSet9();
			bs.unSet(1);
			bs.unSet(3);
			bs.unSet(9);
			bs2.unSet(1);
			bs2.unSet(3);
			bs.equals(bs2).should.equal(false);
		});
	});

	describe('toString()', function() {
		it('toString', function() {
			bs = new BitSet9();
			bs.unSet(4);
			bs.unSet(9);
			bs.toString().should.equal("1235678");
		});
	});
});







