var Cell = require('./src/cell.js');
var Board = require('./src/board.js');
var BitSet9 = require('./src/bitset9.js');
var BruteForceSolver = require('./src/brute-force-solver.js');

var bs = new BitSet9();
bs.unSet(3);
console.log(bs.isSet(4));
//bs.isSet(1).should.equal(true);
//bs.isSet(5).should.equal(true);
//bs.isSet(9).should.equal(true);
console.log(bs.toString());

var b1 = new Board();
console.log(b1.toSudokuString());

// debugger;
var b2 = new Board();
b2.readFromSudokuString("000000001000000002003004000000000350000060000078010000000903400020005000710000000");
console.log(b2.toSudokuString());
console.log(b2.countEmptyCells());

var b3 = new Board();
for (var i = 0; i < 9; i++)
	b3.setCellValue(i,i,i+1);
console.log(b3.toSudokuString());
console.log(b3.countEmptyCells());

var b = new Board();
//var r, c;
//for (r = 0; r < 3; r++)
//	for (c = 0; c < 3; c++)
//		b.setCellValue(r,c,(r*3 + c + 1));
//b.setCellValue(3,3,1);
//b.setCellValue(5,5,9);
//for (r = 6; r < 9; r++)
//	for (c = 6; c < 9; c++)
//		b.setCellValue(r,c,((r-6)*3 + (c-6) + 1));
//console.log(b.toSudokuString());
//console.log(b.countEmptyCells());


cells = [];
cells.push(b.b[4][4]);
cells.push(b.b[1][1]);
cells.push(b.b[8][8]);
for (var i = 2; i <= 9; i++) {
	b.cellRemoveCandidate(4,4,i);
	b.cellRemoveCandidate(1,1,i);
	b.cellRemoveCandidate(8,8,i);
}
s1 = b.map.get(1);
// s1.size.should.equal(3);
s2 = b.map.get(2);
s3 = b.map.get(9);
var cands = b.map.get(1).values();
for (var c = 0; c < cands.length; c++) {
	var cnd = b.lookupCellByRC(cands[c]);
	var cll = cells[c];
	cnd.should.equal(cll);
}

var b5 = new Board();
b5.readFromSudokuString("348000079100008000002000100003060000400907003000020800001000900000600002750000318");
var bff = new BruteForceSolver(b5);
console.log(bff.run());
console.log(b5.toSudokuString());
console.log("348156279169278435572349186213864597485917623697523841821735964934681752756492318 - is correct");

//var b4 = new Board();
//b4.readFromSudokuString("........2..8..7..5.4..2..7.......9...6..9....7...8.....23....5....4.....5..3.9.6.");
//console.log(b4.toSudokuString());
//bff = new BruteForceSolver(b4);
//console.log(bff.run());
//console.log(b4.toSudokuString());

