/**
 * ES6 Set implementation in Node.js 0.11 is missing values().
 * When it is implemented, this Set module can be discarded.
 */

var Set = module.exports = function () {
	this.size = 0;
}

Set.prototype = {
	has : function (key) {this.hasOwnProperty(key); },
	add : function (key) {this[key] = true; this.size++; },
	del : function (key) {delete this[key]; this.size--; },
	values : function () { var result = Object.keys(this); result.splice(0,1); return result; }
}
