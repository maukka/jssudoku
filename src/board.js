//=======================================================================
//Board
var SIZE = 9;
var Cell = require('./cell.js');
var Set = require('./set.js');
var assert = require('assert');

var Board = module.exports = function() {
	this.SIZE = SIZE;
	
	// keys are number of candidates, values are sets of cell pointers
	this.candTable = new Array();	
	
	// Board 9 x 9 of Cells()
	this.b = new Array(SIZE);	
	for (var r = 0; r < SIZE; r++)
		this.b[r] = new Array(SIZE);
	
	init(this);
};

function updateCandTable(board) {
	var r,c,i;

	for (i = 0; i <= SIZE; i++)
		board.candTable[i] = new Set();
	for (r = 0; r < SIZE; r++) {
		for (c = 0; c < SIZE; c++) {
			numc = board.b[r][c].numCandidates();
			if (numc >= 1)
				board.candTable[numc].add(cellIndexToString(r,c));
		}
	}
}

function init(me, source) {
	var r,c;

	for (r = 0; r < SIZE; r++) {
		for (c = 0; c < SIZE; c++) {
			if (source === undefined)
				me.b[r][c] = new Cell();
			else
				me.b[r][c] = source.b[r][c].clone();
		}
	}
	updateCandTable(me);
}

function cellIndexToString(r,c) {
	return r + "-" + c;
};

Board.prototype.lookupCellByRC = function(str) {
	var r = parseInt(str[0],10);
	var c = parseInt(str[2],10);
	return this.b[r][c];
};

Board.prototype.clone = function () {
	var result = new Board();
	init(result, this);
	return result;
};

Board.prototype.copyFrom = function (source) {
	init(this, source);
};

// Two boards equal if all their cells equal. All the rest fields
// are irrelevant.
Board.prototype.equals= function (rhs) {
	var result = true;
	for (var r = 0; r < SIZE; r++) {
		for (var c = 0; c < SIZE; c++) {
			if (!this.b[r][c].equals(rhs.b[r][c]))
				return false;
		}
	}
	return result;
};

Board.prototype.toSudokuString = function () {
	
	var result = this.b.reduce( function (accu1, currCol) {
		return currCol.reduce( function (accu2, currCell) {
			return accu2 + currCell.value;
		}, accu1);
	}, "");
	return result;
};

Board.prototype.readFromSudokuString = function (input) {
	for (var r = 0; r < SIZE; r++) {
		for (var c = 0; c < SIZE; c++) {
			var ch = input[(r * SIZE) + c];
			if (ch == '.')
				ch = '0';
			var i = parseInt(ch, 10);
			if (i > 0)
				this.setCellValue(r, c, i);
		}
	}
	updateCandTable(this);
};

Board.prototype.countEmptyCells = function () {
	var result = this.b.reduce( function (accu1, currCol) {
		return currCol.reduce( function (accu2, currCell) {
			return (currCell.value === 0) ? accu2 + 1 : accu2;
		}, accu1);
	}, 0);
	return result;
};

Board.prototype.cellRemoveCandidate = function (r, c, cand) {
	var cell = this.b[r][c];
	var old_numc = cell.numCandidates();
	if (cell.removeCandidate(cand)) {
		this.candTable[old_numc].del(cellIndexToString(r,c));
		assert.ok(old_numc > 1);
		this.candTable[old_numc - 1].add(cellIndexToString(r,c));
	}
};

// Helper functions to run through inner 3x3 block using index i
function blockR ( r, c, i) {
	var blockBegin = r - (r % 3); 
	return blockBegin + Math.floor(i / 3);
}
function blockC ( r, c, i) {
	var blockBegin = c - (c % 3); 
	return blockBegin + (i % 3);
}

// return true if valid, false on conflicts
// 1. Check that no same value already exists in same row, col or block (internal error)
// 2. Check that reducing candidates on same row, col or block doesn't take last candidate out (normal conflict in branching) 
function validateSetValue( b, row, col, newval) {

	var r, c, i;
	for (r = 0; r < SIZE; r++) {
		if (r === row) continue;
		if (b[r][col].value === newval) return false;
		if (b[r][col].hasCandidate(newval)) {
			if (b[r][col].numCandidates() == 1)
				return false;
		}
	}
	for (c = 0; c < SIZE; c++) {
		if (c === col) continue;
		if (b[row][c].value === newval) return false;
		if (b[row][c].hasCandidate(newval)) {
			if (b[row][c].numCandidates() == 1)
				return false;
		}
	}
	for (i = 0; i < SIZE; i++) {
		var br = blockR(row,col,i);
		var bc = blockC(row,col,i);
		if (br === row && bc === col) continue;
		if (b[br][bc].value === newval) return false;
		if (b[br][bc].hasCandidate(newval)) {
			if (b[br][bc].numCandidates() == 1)
				return false;
		}
	}
	return true;
}

function reduceCandidates( b, row, col, newval) {
	var r, c, i;
	for (r = 0; r < SIZE; r++) {
		if (r === row) continue;
		b[r][col].removeCandidate(newval);
	}
	for (c = 0; c < SIZE; c++) {
		if (c === col) continue;
		b[row][c].removeCandidate(newval);
	}
	for (i = 0; i < SIZE; i++) {
		var br = blockR(row,col,i);
		var bc = blockC(row,col,i);
		if (br === row && bc === col) continue;
		b[br][bc].removeCandidate(newval);
	}
}

// Set new cell value and reduce candidates in the same row, column and block
// Return true if successful, false upon conflict
//
// NOTE! Modifies board, not only the cell !!
//
Board.prototype.setCellValue = function (row, col, newval) {
	assert.ok(row >= 0 && row < 9, "Internal error: row indexes should be 0..8");
	assert.ok(col >= 0 && row < 9, "Internal error: col indexes should be 0..8");
	assert.ok(newval >= 1 && newval <= 9, "Internal error: new cell value should be 1..9");
	assert.ok(!this.b[row][col].isSet(), "Internal error: cell already set");

	// If there is no candidate for newval, then there is conflict. Cannot set.
	if (!this.b[row][col].hasCandidate(newval))
		return false;
	if (!validateSetValue(this.b, row,col,newval))
		return false;
	
	var cell = this.b[row][col];
	var old_numc = cell.numCandidates();
	reduceCandidates( this.b, row, col, newval);
	this.candTable[old_numc].del(cellIndexToString(row,col));
	this.b[row][col].setValue(newval);
	
	return true;
};

