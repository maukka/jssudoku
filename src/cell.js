/**
 * Cell
 */

var BitSet9 = require('./bitset9.js');

var Cell = module.exports = function (newval) {
	if (newval === undefined || newval === 0) {
		this.value = 0;
		this.candidates = new BitSet9(); // already initialized to all bits set
	} else {
		this.value = newval;
		this.candidates = new BitSet9();
		this.candidates.clear(); 
	}
};

Cell.prototype = {
	isSet : function () {
		return (this.value > 0);
	},
	hasCandidate : function (cand) {
		return this.candidates.isSet(cand);
	},
	removeCandidate : function (cand) {
		return this.candidates.unSet(cand);
	},
	numCandidates : function () {
		return this.candidates.count();
	},
	// Return arbitrary candidate
	getNextCandidate : function (prev) {
		return this.candidates.getNext(prev);
	},
	equals : function (rhs) {
		return this.candidates.equals(rhs.candidates) && this.value == rhs.value;
	},
	setValue : function (newval) {
		if (this.isSet())
			throw new EvalError("Cannot set(" + newval + "), cell already set to " + this.value);
		this.value = newval;
		this.candidates.clear(); // empty candidates
	},
	clone : function () {
		var result = new Cell(this.value);
		if (!this.isSet())
			result.candidates = new BitSet9(this.candidates);
		return result;
	}
};
