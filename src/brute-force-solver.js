var Board = require('./board.js');
var Cell = require('./cell.js');
var assert = require('assert');
var iterCounter = 0;

//=======================================================================
// Solver

// Brute-force solver
// return true if solution is found, or false on contradiction
// Solved sudoku is returned in place of b
//
var BruteForceSolver = module.exports = function (b) {
	this.b = b;
};

// branch() will solve sudoku, or fail. Solved sudoku is in place.
function branch(board) {
	for (var r = 0; r < board.SIZE; r++) {
		for (var c = 0; c < board.SIZE; c++) {
			if (!board.b[r][c].isSet()) {
				var cand = board.b[r][c].getNextCandidate();
				while (cand > 0)
				{
					var clone = board.clone();
					if (clone.setCellValue(r,c,cand)) {
						var bfs2 = new BruteForceSolver(clone);
						if (bfs2.run()) {
							board.copyFrom(clone);
							return true;
						}
					}
					// If not solved, discard clone and continue looping
					cand = board.b[r][c].getNextCandidate(cand);
				}
			} 
		}
	}
	return false;
}

BruteForceSolver.prototype.run = function () 
{
	var numEmptyCells;
	while ((numEmptyCells = this.b.countEmptyCells()) > 0)
	{
		// Select cell with least candidates
		var minCandidates = this.b.SIZE + 1;
		var minR = -1, minC = -1;
		for (var r = 0; r < this.b.SIZE; r++) {
			for (var c = 0; c < this.b.SIZE; c++) {
				if (!this.b.b[r][c].isSet() && this.b.b[r][c].numCandidates() < minCandidates)
				{
					minCandidates = this.b.b[r][c].numCandidates();
					minR = r;
					minC = c;
				}
			}
		}
		assert.ok(minCandidates > 0, "BruteForceSolver internal error: minCandidates = 0.");
		assert.ok(minR > -1 && minC > -1, "BruteForceSolver internal error: Cannot find minCandidates.");
	
		var cand = this.b.b[minR][minC].getNextCandidate();
		if (minCandidates > 1) {
			// branch() will solve sudoku, or fail. Solved sudoku is in place.
			if (!branch(this.b))
				return false;
		} else {
			// Only one candidate, set in place and continue looping
			if (!this.b.setCellValue(minR, minC, cand))
				return false;
		}
	}
	
	return true;
};
