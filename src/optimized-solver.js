/**
 * New node file
 */

var Board = require('./board.js');
var Cell = require('./cell.js');
var assert = require('assert');
var iterCounter = 0;

//return true if solution is found, or false on contradiction
//Solved sudoku is returned in place of b
//
var OptimizedSolver = module.exports = function (board) {
	this.b = board;
};

OptimizedSolver.prototype.run = function () {

	if (this.b.countEmptyCells() === 0)
		return true;
	
	// Select cell with least candidates, and recurse through them all
	var s;
	for (var numcand = 1; numcand <= 9; numcand++) {
		s = this.b.candTable[numcand];
		if (s.size > 0)
			break;
	}
	var keys = s.values();
	var rc = keys[0]; // just pick first
	assert.ok(rc.length === 3);
	var r = parseInt(rc[0],10);
	var c = parseInt(rc[2],10);
	var cand = this.b.b[r][c].getNextCandidate();
	assert.ok(cand > 0);
	while (cand > 0)
	{
		var clone = this.b.clone();
		if (clone.setCellValue(r,c,cand)) {
			var os = new OptimizedSolver(clone);
			if (os.run()) {
				this.b.copyFrom(clone);
				return true;
			}
		}
		// If not solved, discard clone and continue looping
		cand = this.b.b[r][c].getNextCandidate(cand);
	}
	
	return false;
};
