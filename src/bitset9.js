/**
 * BitSet9 - 9 bit bitset for counting Sudoku candidates
 * 
 * Notes:
 *   1. new BitSet9() initializes with all 9 bits set
 *   2. set(pos) - pos is from 1 to 9
 *   
 */

var assert = require('assert');

// Initialize with all 9 bits set
var BitSet9 = function (oldBitSet9) {
	if (oldBitSet9 === undefined || !(oldBitSet9 instanceof BitSet9))
		this.bits = 0x1ff;
	else 
		this.bits = oldBitSet9.bits;
};

BitSet9.prototype = {
	clear : function () {
		this.bits = 0;
	},
	unSet : function (pos) {
		assert.ok(pos >= 1 && pos <= 9, "pos must be 1..9");
		if (!this.isSet(pos))
			return false;
		var mask = ~(1 << (pos - 1)); 
		this.bits &= mask;
		return true;
	},
	isSet : function (pos) {
		assert.ok(pos >= 1 && pos <= 9, "pos must be 1..9");
		return (this.bits & (1 << (pos - 1))) > 0; 
	},
	count : function () {
		var result = 0;
		for (var pos = 1; pos <= 9; pos++)
			if (this.isSet(pos))
				result += 1;
		return result;
	},
	getNext : function (prev) {
		if (prev === undefined)
			prev = 0;
		for (var pos = prev + 1; pos <= 9; pos++)
			if (this.isSet(pos))
				return pos;
		return 0;
	},
	equals : function (rhs) {
		return this.bits == rhs.bits;
	},
	toString : function () {
		var result = "";
		for (var pos = 1; pos <= 9; pos++)
			if (this.isSet(pos))
				result += pos.toString();
		return result;
	}
};

module.exports = BitSet9;